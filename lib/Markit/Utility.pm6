unit module Markit::Utility;

#| Check if haystack contains any character from an array of characters.
#| If so, return substring starting from leftmost character found.
#| Else, return False.
sub search-for-characters( Str $haystack, @chars ) is export {
    my $idx = @chars.map({$haystack.index($^needle)}).min;
    return $idx ~~ Int ?? $haystack.substr($idx, *) !! False;
}

#| Replace instances of regex with elements in text. Return an array of
#| elements representing the replacement.
sub regex-replacement-elements( $regex, @elements, $text is copy ) is export {
    my @new-elements;
    while $text.match($regex) {
        my $offset  = $/.from;
        my $before  = $text.substr(0, $offset);
        my $after   = $text.substr($offset + $/.chars);
        @new-elements.push(%(text => $before));

        @new-elements.push($_) for @elements;

        $text = $after;
    }
    @new-elements.push(%(:$text));
}

#| Convert special characters to HTML entities.
sub escape( Str $raw is copy, Bool :$allow-quotes = False ) is export {
    =begin comment
    my @special-chars = [
    "	", "!", "#", '$', "%", "&", "(", ")", "*", "+",  "/", ":",
    ";","<", "=", ">", "?", "@", "[", "\\", "]", "^", "_", "`", '{', "|", '}',
    " ", "¡", "¿", "«", "»", "‐", "–", "—", "―", "¢", "£", "¤", "¥", "§", "®",
    ];

    my @html-entities = [
    "&Tab;", "&excl;", "&num;", "&dollar;", "&percnt;", "&amp;", "&lpar;", "&rpar;",
    "&ast;", "&plus;", "&sol;", "&colon;", "&semi;",
    "&lt;", "&equals;", "&gt;", "&quest;", "&commat;", "&lbrack;", "&bsol;", "&rsqb;",
    "&Hat;", "&lowbar;", "&grave;", "&lbrace;", "&vert;", "&rcub;", "&nbsp;", "&iexcl;",
    "&iquest;", "&laquo;", "&raquo;", "&hyphen;", "&ndash;", "&mdash;", "&horbar;", "&cent;",
    "&pound;", "&curren;", "&yen;", "&sect;", "&reg;"
    ];
    =end comment

    my @special-chars = ['&', '<', '>'];
    my @html-entities = ['&amp;','&lt;', '&gt;'];

    $raw .= trans(@special-chars => @html-entities);
    unless $allow-quotes {
        $raw .= trans([q{"}, q{'}] => ['&quot;','&#039;'])
    }

    return $raw;
}

#| Finds the length of a string's initial segment consisting entirely
#| of characters contained within a given mask. See strcspn.
sub strspn(
    Str $subject,
    Str $mask,
    Int $start = 0,
    Int $length? is copy
) is export {
    if $mask.contains(' ') {
        note 'To include whitespace in the mask, use \s instead.'
    }

    # Workaround for lack of variable interpolation inside character classes
    # https://www.nntp.perl.org/group/perl.perl6.users/2019/09/msg6973.html
    sub matching-chars( Str $string, Str $mask ) {
        given "<[$mask]>" {
            return $string.match(/^ <$_>+ /);
        }
    }

    $length    = $subject.chars unless $length.defined;
    my $substr = $subject.substr($start, $length);
    my $match  = matching-chars($substr, $mask);
    return $match eqv Any ?? 0 !! $match.Str.chars;
}

#| Finds the length of a string's initial segment that doesn't match
#| the mask. See strspn.
sub strcspn(
    Str $subject,
    Str $mask,
    Int $start = 0,
    Int $length? is copy
) is export {
    if $mask.contains(' ') {
        note 'To include whitespace in the mask, use \s instead.'
    }

    # Workaround for lack of variable interpolation inside character classes
    # https://www.nntp.perl.org/group/perl.perl6.users/2019/09/msg6973.html
    sub matching-chars( Str $string, Str $mask ) {
        given "<-[$mask]>" {
            return $string.match(/^ <$_>+ /);
        }
    }

    $length    = $subject.chars unless $length.defined;
    my $substr = $subject.substr($start, $length);
    my $match  = matching-chars($substr, $mask);
    return $match eqv Any ?? 0 !! $match.Str.chars;
}

#| Count the number of substring occurrences.
sub substr-count(
    Str:D $haystack,
    Str:D $needle,
    Int $offset = 0,
    Int $length? is copy
    --> Int
) is export {
    if $length.defined {
        X::OutOfRange.new(
            what  => "Offset",
            got   => $offset + $length,
            range => "offset + length ≤ haystack's length"
        ).throw if $offset + $length > $haystack.chars;
    }
    else {
        $length = $haystack.chars - $offset;
    }

    return $haystack.substr($offset, $length).match(/$needle/, :global).Int
}

#| Strip whitespace (or mask characters) from the end of a string
#| and return modified string. Pass the :front flag to strip from
#| the front of the string.
sub clip( Str $string, $mask?, Bool :$front = False ) is export {
    if $mask.defined {
        given "<[$mask]>" {
            return $front
            ?? $string.subst(/^ <$_>+ /, '')
            !! $string.subst(/ <$_>+ $/, '')
        }
    }
    return $front
    ?? $string.subst(/^ <[\s\t\n\v\h]>+/, '')
    !! $string.subst(/<[\s\t\n\v\h]>+ $/, '')
}

#| Return the substring before a given character (exclusive) or after it (inclusive).
#| Return the haystack without needle and the empty string if the needle isn't
#| found in the haystack. Die if both :after and :before are passed.
sub get-substring(
    Str $haystack,
    Str :before($n),
    Str :after($m)
    --> Str
) is export {
    die "Either pass :after or :before" if all($n, $m);
    return $haystack unless one($n, $m);

    my $needle = $n // $m;
    my $offset = $haystack.index($needle);

    return '' unless $offset;

    given $haystack {
        if $n { .substr(0, $offset) } # before needle (exclude it)
        else  { .substr($offset, *) } # after needle (include it)
    }
}
