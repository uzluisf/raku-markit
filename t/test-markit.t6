use v6;
use Test;
use Markit;
use lib 'lib';

my $data = 't/data'.IO;

my %if-sets = %(
    strict => <
        strict-atx-heading.md
    >,

    safe => <
        xss-bad-url.md
        xss-text-encoding.md
    >,
);

for dir($data, test => { .ends-with('md') }).sort -> $md-fh {
    my $html-fh = $data.add($md-fh.basename.split('.').first ~ '.html').IO;

    unless $html-fh.f {
        note "No corresponding «{$html-fh.basename}» for «{$md-fh.basename}». Skipping!";
        next;
    }

    my $markdown-text  = $md-fh.slurp;
    my $expected-html  = $html-fh.slurp.chomp;

    $markdown-text .= subst(/<[\r\n \r]>/, "\n", :g);

    my $md-obj      = Markdown.new;

    given $md-obj, $md-fh.basename -> ($o, $f) {
        when $f eq %if-sets<strict>.one { $o.strict-mode = True; proceed }
        when $f eq %if-sets<safe>.one   { $o.safe-mode   = True; proceed  }
    }

    my $actual-html = $md-obj.markdown($markdown-text);

    is $actual-html, $expected-html, "{$md-fh.basename}";
}

done-testing;
